#andaily-developer

andaily-developer, 源自<a href="http://a.andaily.com">andaily</a>开发过程中使用的scrum工具.

<hr/>
<h4>版本</h4>
<ul>
    <li>2014-06-18    <a href="http://git.oschina.net/mkk/andaily-developer/tree/ad_0.1/">ad_0.1</a></li>
    <li>2014-07-22   <a href="http://git.oschina.net/mkk/andaily-developer/tree/ad_0.5_i18n/">ad_0.5_i18n</a></li>
    <li>2015-04-07   <a href="http://git.oschina.net/mkk/andaily-developer/tree/ad_0.6/">ad_0.6</a></li>
</ul>

<p>
      <h4>运行环境</h4>
      <ol>
             <li>JDK 1.7 +</li>
             <li>MySql 5.5 +</li>
             <li>Tomcat 7 +</li>
             <li>Maven 3.1.0 +</li>
      </ol>
</p>

<p>
      <h4>角色定义</h4>
      在系统中定义了4种角色: Super Man, Product Owner, Scrum Master 与 Scrum Member, 介绍如下
      <ol>
             <li>Super Man(类似系统管理员), 在系统中管理用户(Developer), 团队(Team), 项目(Project) 与系统设置(尚未实现), 系统在安装时会初始化一个Super Man 用户信息.
                不参与Scrum的流程, 只定义基础数据.
             </li>
             <li>Product Owner(产品负责人), 管理项目(Project), Backlog与Sprint, 监控Sprint的执行情况, 保证Sprint达到预期目标</li>
             <li>Scrum Master(团队负责人), 可管理Backlog与Sprint, task等, 负责团队的日常监控与管理, 同时也参与任务的执行</li>
             <li>Scrum Member(团队成员), 主要是执行Sprint的具体任务, 保证每一个任务按照完成</li>
      </ol>
</p>


<hr/>

<p>
    <h4>Online Testing</h4>
    Test Server URL: <a href="https://andaily.com/d/">https://andaily.com/d/</a>
    <br/>
    <table>
        <tr>
            <th>Role</th>
            <th>Username</th>
            <th>Password</th>
        </tr>
        <tr>
            <td>Product Owner</td>
            <td>product-owner@andaily.com</td>
            <td>andaily</td>
        </tr>
        <tr>
            <td>Scrum Master</td>
            <td>master@andaily.com</td>
            <td>andaily</td>
        </tr>
        <tr>
            <td>Scrum Member</td>
            <td>member@andaily.com</td>
            <td>andaily</td>
        </tr>
        <tr>
            <td>Scrum Member</td>
            <td>member2@andaily.com</td>
            <td>andaily</td>
        </tr>
    </table>
</p>

<hr/>
<p>
    <h4>如何使用?</h4>
    <ol>
        <li>
            项目是Maven管理的, 需要本地安装maven(开发用的maven版本号为3.1.0), 还有MySql(开发用的mysql版本号为5.5)
        </li>
        <li>
            下载(或clone)项目到本地
        </li>
        <li>
            创建MySQL数据库(如数据库名andaily_developer), 并运行相应的SQL脚本(脚本文件位于others/database/ddl目录),
            <br/>
            运行脚本的顺序: init-db.sql -> init-data.sql
        </li>
        <li>
            修改andaily-developer.properties(位于src/resources目录)中的配置信息(数据库连接,邮件配置,文件上传路径)
        </li>
        <li>
            将本地项目导入到IDE(如Intellij IDEA)中,配置Tomcat(或类似的servlet运行服务器), 并启动Tomcat(默认端口为8080)
            <br/>
           另: 也可通过maven package命令将项目编译为war文件(ad.war),
                 将war放在Tomcat中并启动(注意: 这种方式需要将andaily-developer.properties加入到classpath中并正确配置数据库连接信息等).
        </li>
        <li>
            浏览器器访问 http://localhost:8080/ad/ 进入登录, 初始的用户名/密码为: admin@andaily.com/admin (详见/others/database/ddl/init-data.sql文件)
            <br/>
            登录成功,开始scrum之旅...
        </li>
    </ol>
</p>


<hr/>
<h3>开发计划</h3>
<p>
从 1.0 版本开始将项目的所有计划的开发内容列出来, 方便大家跟进, 也欢迎你加入.
<br/>
项目的开发管理使用开源项目 <a href="http://git.oschina.net/mkk/andaily-developer">andaily-developer</a>.
</p>
<ul>
       <li>
            <p>
                Version: <strong>1.0</strong> [pending]
                <br/>
                Date: 2016-10-24 / ------
            </p>
            <ol>
                <li><p>#57 - 检查所有 popover 如果 data-content 有特殊字符将显示错误</p></li>
                <li><p>#72 - When click 'Do it' menu, then popup confirm message add task number in the message.</p></li>
                <li><p><del>#81 - If the backlog type is 'BUG'; use red color for notice</del></p></li>
                <li><p>#82 - 如果项目指定了结束日期(deadline),则在创建sprint需要检查日期不能在deadline之后(在创建时要检查,选择sprint deadline时也要检查)</p></li>
                <li><p>#88 - 对于已经结束的项目(结束日期在当前日期之前).在创建Sprint,Backlog时要有所区别,如使用option group....</p></li>
                <li><p>#90 - 若已完成的任务有备注信息,则应该在列表中有所提示(如添加一个icon)</p></li>
                <li><p>#163 - User添加自己的时间统计,按天统计在各项目中的时间报告 </p></li>
                <li><p>#164 - 每个项目增加时间统计报表,按照时间排序 </p></li>
            </ol>
       </li>

</ul>
<br/>


<hr/>

<p>
    <h4>Change Log</h4>
    <ol>
        <li>
            <p>
                2013-10-10   ----    Import codes
            </p>
        </li>
        <li>
            <p>
                2014-07-22   ----    Publish <em>ad_0.5_i18n</em> branch
            </p>
        </li>
        <li>
            <p>
                2014-12-09   ----    Fix duplicate task-number issues...
            </p>
        </li>
        <li>
            <p>
                2015-02-13   ----    Deploy the latest project to Test Server
            </p>
        </li>
        <li>
            <p>
                2015-03-16   ----    Start to develop 0.6 version Sprint
            </p>
        </li>
        <li>
            <p>
                2015-04-07   ----    Publish <em>ad_0.6</em> branch
            </p>
        </li>
        <li>
            <p>
                2015-10-24   ----    Start develop <em>1.0</em> branch
            </p>
        </li>
    </ol>
</p>

<hr/>
<h4>程序主要功能截图与说明</h4>
<ol>
    <li>
        Sprint 任务列表, 包括任何的创建, 分配, 时间估算, 燃尽图, 以及任何的完成, 移动, 删除, 重置等等, 开发时使用频率极高.
        <br/>
        <img src="http://andaily.qiniudn.com/0.1-sprint_task.jpg" alt="sprint_tasks" style="max-width:800px; "/>
    </li>
    <li>
        Sprint 会议记录, 记录Sprint执行过程中的各种会议(Daily standing, Sprint planning, Sprint review, Retrospective)内容.
         <br/>
        <img src="http://andaily.qiniudn.com/0.1-sprint_meeting.jpg" alt="sprint_meetings" style="max-width:800px; "/>
    </li>
    <li>
        Sprint 列表, 展示项目的所有Sprint信息, 整体进度等信息
        <br/>
        <img src="http://andaily.qiniudn.com/0.1-sprints.jpg" alt="sprints" style="max-width:600px; "/>
    </li>
    <li>
        Backlog 列表,  管理项目的backlog, 主要由 Product Owner 操作 <br/>
        <img src="http://andaily.qiniudn.com/0.1-backlogs.jpg" alt="backlogs" style="max-width:600px; "/>
    </li>
    <li>
        Sprint 时间报表, 实时反映sprint的时间信息与统计数据.
        <br/>
        <img src="http://andaily.qiniudn.com/0.1-time_report.jpg" alt="sprint_form" style="max-width:800px; "/>
    </li>
    <li>
       Sprint 燃尽图
        <br/>
        <img src="http://andaily.qiniudn.com/0.1-burn_down.jpg" alt="ad"/>
    </li>
</ol>

<hr/>

<h4>用户, 团队与项目 部分</h4>

<ol>
    <li>
       用户管理, 每个用户必须选择四种角色中的一种.
        <br/>
        <img src="http://andaily.qiniudn.com/0.1-users.jpg" alt="ad"/>
    </li>
    <li>
       团队管理, 每个团队至少一名Scrum master与一名Scrum member, 团队必须关联一个或多个项目
        <br/>
        <img src="http://andaily.qiniudn.com/0.1-teams.jpg" alt="ad"/>
    </li>
    <li>
       项目管理, 由Super man与Product Owner维护, 项目必须关联一个或多个Product Owner, 并设定项目结束日期.
        <br/>
        <img src="http://andaily.qiniudn.com/0.1-projects.jpg" alt="ad"/>
    </li>

</ol>



<hr/>
<p>
    欢迎关注 Andaily 项目, GIT访问地址: <a href="https://git.oschina.net/mkk/andaily">https://git.oschina.net/mkk/andaily</a>
    <br/>
    Andaily 在线访问地址: <a href="https://andaily.com/andaily/" target="_blank">https://andaily.com/andaily/</a>
    <br/>
    更多开源项目请访问: <a href="https://andaily.com/my_projects.html">https://andaily.com/my_projects.html</a>
</p>